package com.tgss.mdmcoaoclud1mmpnproducer.model;

import java.math.BigInteger;
import java.util.Date;

public class MDMCoupaCoaDTO {
    private Long id;
    private BigInteger coupaId;
    private String coaCode;
    private String coaName;
    private String companyCode;
    private Boolean isActive;
    private String coaType;
    private String companyName;
    private String coaDescription;
    private Date createdAt;
    private Date updatedAt;
    private String createdBy;
    private String updatedBy;
    private Long parent;
    private String lookupParentCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getCoupaId() {
        return coupaId;
    }

    public void setCoupaId(BigInteger coupaId) {
        this.coupaId = coupaId;
    }

    public String getCoaCode() {
        return coaCode;
    }

    public void setCoaCode(String coaCode) {
        this.coaCode = coaCode;
    }

    public String getCoaName() {
        return coaName;
    }

    public void setCoaName(String coaName) {
        this.coaName = coaName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getCoaType() {
        return coaType;
    }

    public void setCoaType(String coaType) {
        this.coaType = coaType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCoaDescription() {
        return coaDescription;
    }

    public void setCoaDescription(String coaDescription) {
        this.coaDescription = coaDescription;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public String getLookupParentCode() {
        return lookupParentCode;
    }

    public void setLookupParentCode(String lookupParentCode) {
        this.lookupParentCode = lookupParentCode;
    }
}
