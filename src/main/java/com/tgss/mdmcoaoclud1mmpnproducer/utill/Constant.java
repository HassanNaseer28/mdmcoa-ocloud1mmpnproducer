package com.tgss.mdmcoaoclud1mmpnproducer.utill;

public interface Constant {
    public static final String PARAM_LOOKUP_NAME = "lookupName";
    public static final String PARAM_COMPANY_CODE = "companyCode";
    public static final String PARAM_COUNTRY_CODE = "countryCode";
    String STATUS = "PENDING";

    String SERVICE_NAME = "MDMCoupaCOADataService";

    public class TargetURI {
        public static final String URI_HOLD_TRX = "/processHoldCoaTrx";
        public static final String URI_LOOKUP = "/saveAndUpdateCoa";
        public static final String URI_LOOKUP_STG = "/saveAndUpdateCoaStg";

    }

    public static class Status {

        public static final String UPDATED = "UPDATED";
        public static final String CREATED = "CREATED";
        public static final String RETRIEVED = "RETRIEVED";
        public static final String ERROR = "ERROR";
    }
    public enum HTTPStatusCode {
        CREATED(201),
        SUCCESSFUL(200);

        private int httpCode;

        HTTPStatusCode(int httpCode) {
            this.httpCode = httpCode;
        }

        public int httpCode() {
            return this.httpCode;
        }

        @Override
        public String toString() {
            return String.valueOf(httpCode());
        }
    }
}
