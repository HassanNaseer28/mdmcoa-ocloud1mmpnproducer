package com.tgss.mdmcoaoclud1mmpnproducer.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author hassan.naseer
 */
@Component
@PropertySource("classpath:i2framework.properties")
public class AppProperties {
    @Value(value = "${kafka.bootstrapAddress}")
    private String bootstrapAddress;

    @Value(value = "${lookup.consumer.topic.name}")
    private String lookupTopicName;

    @Value(value = "${com.tgss.i2.capability.logging.buName}")
    private String buName;

    @Value(value = "${com.tgss.i2.capability.logging.integrationName}")
    private String integrationName;

    @Value(value = "${mdm.coupa.coa.ds.url}")
    private String mdmCoupaCOADSURL;

    @Value(value = "${com.tgss.i2.capability.logging.serviceName}")
    private String serviceName;

    @Value(value = "${com.tgss.i2.capability.logging.source}")
    private String source;

    @Value(value = "${com.tgss.i2.capability.logging.sourceInterface}")
    private String sourceInterface;

    public String getBootstrapAddress() {
        return bootstrapAddress;
    }

    public void setBootstrapAddress(String bootstrapAddress) {
        this.bootstrapAddress = bootstrapAddress;
    }

    public String getLookupTopicName() {
        return lookupTopicName;
    }

    public void setLookupTopicName(String lookupTopicName) {
        this.lookupTopicName = lookupTopicName;
    }

    public String getBuName() {
        return buName;
    }

    public void setBuName(String buName) {
        this.buName = buName;
    }

    public String getIntegrationName() {
        return integrationName;
    }

    public void setIntegrationName(String integrationName) {
        this.integrationName = integrationName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceInterface() {
        return sourceInterface;
    }

    public void setSourceInterface(String sourceInterface) {
        this.sourceInterface = sourceInterface;
    }

    public String getMdmCoupaCOADSURL() {
        return mdmCoupaCOADSURL;
    }

    public void setMdmCoupaCOADSURL(String mdmCoupaCOADSURL) {
        this.mdmCoupaCOADSURL = mdmCoupaCOADSURL;
    }
}
