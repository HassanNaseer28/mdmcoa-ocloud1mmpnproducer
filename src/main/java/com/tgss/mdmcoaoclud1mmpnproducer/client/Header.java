package com.tgss.mdmcoaoclud1mmpnproducer.client;

import com.tgss.i2.core.framework.model.dto.Payload;
import com.tgss.mdmcoaoclud1mmpnproducer.model.MDMCoupaCoaDTO;
import com.tgss.mdmcoaoclud1mmpnproducer.model.MDMCoupaCoaTrxDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;


/**
 *
 * @author Hassan.Naseer
 */
@Component
public class Header {



    public HttpHeaders headerConfig() {

        HttpHeaders httpHeader = new HttpHeaders();
        httpHeader.set("Accept", "application/json");
        httpHeader.set("Content-type", "application/json");

        return httpHeader;
    }


    public HttpEntity<MDMCoupaCoaTrxDTO> setHttpEntity(MDMCoupaCoaTrxDTO mdmCoupaCoaTrxDTO) {
        return new HttpEntity<MDMCoupaCoaTrxDTO>(mdmCoupaCoaTrxDTO,this.headerConfig());
    }

    public HttpEntity<MDMCoupaCoaDTO> setHttpEntity(MDMCoupaCoaDTO mdmCoupaCoaDTO) {
        return new HttpEntity<MDMCoupaCoaDTO>(mdmCoupaCoaDTO,this.headerConfig());
    }

    public HttpEntity<Payload> setHttpEntity(Payload payload) {
        return new HttpEntity<Payload>(payload,this.headerConfig());
    }

    public HttpEntity<String> getHttpEntity() {
        return new HttpEntity<>(this.headerConfig());
    }
}
