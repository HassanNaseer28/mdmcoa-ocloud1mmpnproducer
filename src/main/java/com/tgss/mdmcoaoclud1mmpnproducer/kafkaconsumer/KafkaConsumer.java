package com.tgss.mdmcoaoclud1mmpnproducer.kafkaconsumer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tgss.i2.core.framework.aspect.interfaces.LogKafkaConsumer;
import com.tgss.i2.core.framework.kafka.producer.KafkaProducer;
import com.tgss.i2.core.framework.model.LogInfo;
import com.tgss.i2.core.framework.model.Response;
import com.tgss.i2.core.framework.model.dto.Payload;
import com.tgss.i2.core.framework.util.Constants;
import com.tgss.i2.core.framework.util.LoggingUtil;
import com.tgss.i2.core.framework.util.PayloadMapper;
import com.tgss.mdmcoaoclud1mmpnproducer.client.RestClient;
import com.tgss.mdmcoaoclud1mmpnproducer.config.AppProperties;
import com.tgss.mdmcoaoclud1mmpnproducer.model.MdmPnCoaStgEntity;
import com.tgss.mdmcoaoclud1mmpnproducer.utill.Constant;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;


/*
@author - HassanNaseer on 8/19/2020
*/
@Component
public class KafkaConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

    @Autowired
    private LoggingUtil loggingUtil;

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private ObjectMapper objectMapper;

    @Value(value = "${notification.topic.name}")
    private String notificationTopicName;

    @Autowired
    private KafkaProducer kafkaProducer;

    @Autowired
    private RestClient restClient;


    Response response = null;

    private final CountDownLatch latch = new CountDownLatch(1);

    @KafkaListener(topics = "${lookup.consumer.topic.name}", containerFactory = "invoiceKafkaListenerContainerFactory")
    @LogKafkaConsumer
    public void listenerMethod(ConsumerRecord<?, ?> consumerRecord) {
        Payload payload = null;

        try {
            LOGGER.info("Lookup request received from PN!!");
            payload = (Payload) consumerRecord.value();

            //Kafka Logging
            LogInfo logInfo = new LogInfo.LogInfoKafkaConsumerBuilder(appProperties.getLookupTopicName(), null, payload.getHeader().getUid(), payload.getHeader().getAppContentID(), Constants.Status.SUCCESS).build();
            loggingUtil.logKafkaConsumer(logInfo);

            LOGGER.info("Payload Received at Kafka : " + PayloadMapper.convertObjectToJson(payload));
            saveCOATOStaging(payload);

        } catch (Exception ex) {
            LOGGER.error("Exception ex : " + ex.getMessage());
            LOGGER.error(ex.getMessage());
            response = new Response();
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.EXC);
        }
        this.latch.countDown();
    }

    public void saveCOATOStaging(Payload payload) throws Exception {
        Object obj = payload.getBody().getObject();
        Response response = new Response();
        response.setObject(obj);

        LOGGER.info("Response Status: " + response.getStatus());
        LOGGER.info("Response Message: " + response.getMessage());

        if (response.getStatus() != null
                && !response.getStatus().equals(Constants.Status.ERROR)) {
            updateLastRunAt(payload);

            if (response.getObject() != null) {
                String lookupType = payload.getHeader().getPromotedFields().get(Constant.PARAM_LOOKUP_NAME);
                String companyCode = payload.getHeader().getPromotedFields().get(Constant.PARAM_COMPANY_CODE);
                String countryCode = payload.getHeader().getPromotedFields().get(Constant.PARAM_COUNTRY_CODE);
                MdmPnCoaStgEntity mdmAtlasCoaStgEntity = new MdmPnCoaStgEntity();
                mdmAtlasCoaStgEntity.setLeCode(companyCode);
                mdmAtlasCoaStgEntity.setRequest(objectMapper.writeValueAsString(response.getObject()));
                mdmAtlasCoaStgEntity.setStatus(Constant.STATUS);
                mdmAtlasCoaStgEntity.setLookupType(lookupType);
                mdmAtlasCoaStgEntity.setCountryCode(countryCode);
                payload.getBody().setObject(mdmAtlasCoaStgEntity);
                restClient.saveRecordInSTG(payload);
            } else {
                LOGGER.info("Lookup values not found for this call!!");
            }
        }
    }

    private void updateLastRunAt(Payload payload) {
        try {
            Map<String, String> promotedFields = payload.getHeader().getPromotedFields();
            promotedFields.put(Constants.PromotedFieldKeys.BU_NAME, appProperties.getBuName());
            promotedFields.put(Constants.PromotedFieldKeys.INTEGRATION_NAME, appProperties.getIntegrationName());
            promotedFields.put(Constants.PromotedFieldKeys.SERVICE_NAME, appProperties.getServiceName());
            promotedFields.put(Constants.PromotedFieldKeys.SOURCE, appProperties.getSource());
            promotedFields.put(Constants.PromotedFieldKeys.SOURCE_INTERFACE, appProperties.getSourceInterface());
            promotedFields.put(Constants.PromotedFieldKeys.TARGET, "TOPIC");
            promotedFields.put(Constants.PromotedFieldKeys.TARGET_INTERFACE, notificationTopicName);
            promotedFields.put(Constants.PromotedFieldKeys.REQUEST_ID, UUID.randomUUID().toString());

            payload.getHeader().setPromotedFields(promotedFields);

            kafkaProducer.pushPayload(payload, notificationTopicName);
        } catch (Exception ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while update LastRunAt");
            loggingUtil.logExceptionFromPayload(payload, ex);
        }
    }

 }
