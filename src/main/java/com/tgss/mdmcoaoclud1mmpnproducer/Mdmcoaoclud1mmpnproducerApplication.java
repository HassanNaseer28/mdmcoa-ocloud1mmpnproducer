package com.tgss.mdmcoaoclud1mmpnproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.tgss.*")
public class Mdmcoaoclud1mmpnproducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Mdmcoaoclud1mmpnproducerApplication.class, args);
	}

}
