package com.tgss.mdmcoaoclud1mmpnproducer.model;

public class MDMCoupaCoaTrxDTO extends MDMCoupaCoaDTO {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MDMCoupaCoaTrxDTO{" +
                "status='" + status + '\'' +
                '}';
    }
}
