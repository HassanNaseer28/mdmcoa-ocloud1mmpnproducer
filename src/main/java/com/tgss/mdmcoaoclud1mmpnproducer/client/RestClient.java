package com.tgss.mdmcoaoclud1mmpnproducer.client;

import com.tgss.i2.core.framework.model.LogInfo;
import com.tgss.i2.core.framework.model.Response;
import com.tgss.i2.core.framework.model.dto.Payload;
import com.tgss.i2.core.framework.util.Constants;
import com.tgss.i2.core.framework.util.LoggingUtil;
import com.tgss.mdmcoaoclud1mmpnproducer.config.AppProperties;
import com.tgss.mdmcoaoclud1mmpnproducer.model.MDMCoupaCoaTrxDTO;
import com.tgss.mdmcoaoclud1mmpnproducer.utill.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


@Component("customRestClient")
public class RestClient {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private AppProperties appProperties;
    @Autowired
    private Header header;
    @Autowired
    private LoggingUtil loggingUtil;

    @Retryable(
            value = {Exception.class},
            maxAttemptsExpression = "${rest.retry.attempts}", backoff = @Backoff(delayExpression = "${rest.retry.delay}"))
    public Response saveRecordInTrx(Payload payload, MDMCoupaCoaTrxDTO mdmCoupaCoaTrxDTO) throws Exception {
        Response response = new Response();
        HttpEntity<MDMCoupaCoaTrxDTO> entity = header.setHttpEntity(mdmCoupaCoaTrxDTO);
        ResponseEntity<Response> responseEntity = null;

        try {
            String url = appProperties.getMdmCoupaCOADSURL() + Constant.TargetURI.URI_LOOKUP;
            System.out.println("Lookup Data Service URL: " + url);
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, Response.class);
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.TARGET, "MDMCoupaCoaDataService");
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.TARGET_INTERFACE, url);

            if (Constant.HTTPStatusCode.SUCCESSFUL.httpCode() == responseEntity.getStatusCode().value()) {
                response.setObject(responseEntity.getBody());
                response.setMessage(Constant.Status.RETRIEVED);
                response.setStatus(Constants.Status.SUCCESS);
            } else {
                response.setMessage(String.valueOf(responseEntity.getStatusCode()));
                response.setMessage(Constant.Status.ERROR);
            }
        } catch (HttpStatusCodeException ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while saveRecordInTrx");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getResponseBodyAsString());
            response.setStatus(Constants.Status.ERROR);
        } catch (RestClientException ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while saveRecordInTrx");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.ERROR);
        } catch (Exception ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while saveRecordInTrx");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.ERROR);
        }
        logRequest(response, payload, Constants.HttpTargetMethods.POST + Constant.TargetURI.URI_LOOKUP);

        if (!response.getStatus().equals(Constants.Status.SUCCESS)) {
            throw new Exception(response.getMessage());
        }

        return response;
    }

    @Retryable(
            value = {Exception.class},
            maxAttemptsExpression = "${rest.retry.attempts}", backoff = @Backoff(delayExpression = "${rest.retry.delay}"))
    public Response invokeHoldTrxService(Payload payload) throws Exception {
        Response response = new Response();
        HttpEntity<String> entity = header.getHttpEntity();
        ResponseEntity<Object> responseEntity = null;

        try {
            String url = appProperties.getMdmCoupaCOADSURL() + Constant.TargetURI.URI_HOLD_TRX;
            System.out.println("URL: " + url);
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, Object.class);
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.TARGET, "MDMCoupaCoaDataService");
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.TARGET_INTERFACE, url);

            if (Constant.HTTPStatusCode.SUCCESSFUL.httpCode() == responseEntity.getStatusCode().value()) {
                response.setObject(responseEntity.getBody());
                response.setMessage(Constant.Status.RETRIEVED);
                response.setStatus(Constants.Status.SUCCESS);
            } else {
                response.setMessage(String.valueOf(responseEntity.getStatusCode()));
                response.setMessage(Constant.Status.ERROR);
            }
        } catch (HttpStatusCodeException ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while invokeHoldTrxService");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getResponseBodyAsString());
            response.setStatus(Constants.Status.ERROR);
        } catch (RestClientException ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while invokeHoldTrxService");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.ERROR);
        } catch (Exception ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while invokeHoldTrxService");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.ERROR);
        }
        logRequest(response, payload, Constants.HttpTargetMethods.POST + Constant.TargetURI.URI_HOLD_TRX);

        if (!response.getStatus().equals(Constants.Status.SUCCESS)) {
            throw new Exception(response.getMessage());
        }
        return response;
    }

    @Retryable(
            value = {Exception.class},
            maxAttemptsExpression = "${rest.retry.attempts}", backoff = @Backoff(delayExpression = "${rest.retry.delay}"))
    public Response saveRecordInSTG(Payload payload) throws Exception {
        Response response = new Response();
        HttpEntity<Payload> entity = header.setHttpEntity(payload);
        ResponseEntity<Response> responseEntity = null;

        try {
            String url = appProperties.getMdmCoupaCOADSURL() + Constant.TargetURI.URI_LOOKUP_STG;
            System.out.println("Lookup Data Service URL: " + url);
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, Response.class);
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.TARGET, "MDMCoupaCoaDataService");
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.TARGET_INTERFACE, url);

            if (Constant.HTTPStatusCode.SUCCESSFUL.httpCode() == responseEntity.getStatusCode().value()) {
                response.setObject(responseEntity.getBody());
                response.setMessage(Constant.Status.CREATED);
                response.setStatus(Constants.Status.SUCCESS);
            } else {
                response.setMessage(String.valueOf(responseEntity.getStatusCode()));
                response.setMessage(Constant.Status.ERROR);
            }
        } catch (HttpStatusCodeException ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while saveRecordInSTG");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getResponseBodyAsString());
            response.setStatus(Constants.Status.ERROR);
        } catch (RestClientException ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while saveRecordInSTG");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.ERROR);
        } catch (Exception ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while saveRecordInSTG");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.ERROR);
        }
        logRequest(response, payload, Constants.HttpTargetMethods.POST + Constant.TargetURI.URI_LOOKUP_STG);

        if (!response.getStatus().equals(Constants.Status.SUCCESS)) {
            throw new Exception(response.getMessage());
        }

        return response;
    }

    @Retryable(
            value = {Exception.class},
            maxAttemptsExpression = "${rest.retry.attempts}", backoff = @Backoff(delayExpression = "${rest.retry.delay}"))
    public Response updateRecordInSTG(Payload payload) throws Exception {
        Response response = new Response();
        HttpEntity<Payload> entity = header.setHttpEntity(payload);
        ResponseEntity<Response> responseEntity = null;

        try {
            String url = appProperties.getMdmCoupaCOADSURL() + Constant.TargetURI.URI_LOOKUP_STG;
            System.out.println("Lookup Data Service URL: " + url);
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, Response.class);
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.TARGET, "MDMCoupaCoaDataService");
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.TARGET_INTERFACE, url);

            if (Constant.HTTPStatusCode.SUCCESSFUL.httpCode() == responseEntity.getStatusCode().value()) {
                response.setObject(responseEntity.getBody());
                response.setMessage(Constant.Status.UPDATED);
                response.setStatus(Constants.Status.SUCCESS);
            } else {
                response.setMessage(String.valueOf(responseEntity.getStatusCode()));
                response.setMessage(Constant.Status.ERROR);
            }
        } catch (HttpStatusCodeException ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while updateRecordInSTG");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getResponseBodyAsString());
            response.setStatus(Constants.Status.ERROR);
        } catch (RestClientException ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while updateRecordInSTG");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.ERROR);
        } catch (Exception ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while updateRecordInSTG");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.ERROR);
        }
        logRequest(response, payload, Constants.HttpTargetMethods.PUT + Constant.TargetURI.URI_LOOKUP_STG);

        if (!response.getStatus().equals(Constants.Status.SUCCESS)) {
            throw new Exception(response.getMessage());
        }

        return response;
    }

    public Response getRecordsFromSTG(Payload payload) {
        Response response = new Response();
        HttpEntity<String> entity = header.getHttpEntity();
        ResponseEntity<Response> responseEntity = null;

        try {
            String url = appProperties.getMdmCoupaCOADSURL() + Constant.TargetURI.URI_LOOKUP_STG + "?status=" + Constant.STATUS;
            System.out.println("Lookup Data Service URL: " + url);
            responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, Response.class);
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.TARGET, "MDMCoupaCoaDataService");
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.TARGET_INTERFACE, url);

            if (Constant.HTTPStatusCode.SUCCESSFUL.httpCode() == responseEntity.getStatusCode().value()) {
                if (responseEntity.getBody() != null) {
                    response = responseEntity.getBody();
                } else {
                    response.setStatus(Constants.Status.SUCCESS);
                    response.setMessage(String.valueOf(Constants.StatusMessage.NO_DATA_FOUND));
                }
            } else {
                response.setMessage(String.valueOf(responseEntity.getStatusCode()));
                response.setMessage(Constant.Status.ERROR);
            }
        } catch (HttpStatusCodeException ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while getRecordsFromSTG");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getResponseBodyAsString());
            response.setStatus(Constants.Status.ERROR);
        } catch (RestClientException ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while getRecordsFromSTG");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.ERROR);
        } catch (Exception ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception while getRecordsFromSTG");
            loggingUtil.logExceptionFromPayload(payload,ex);
            response.setMessage(ex.getMessage());
            response.setStatus(Constants.Status.ERROR);
        }
        logRequest(response, payload, Constants.HttpTargetMethods.GET + Constant.TargetURI.URI_LOOKUP_STG);

        return response;
    }

    private void logRequest(Response response, Payload payload, String uri) {
        try {
            //Kibana logging
            LogInfo logInfo = new LogInfo.LogInfoWSCallSentBuilder(Constant.SERVICE_NAME, uri, response.getMessage(), payload.getHeader().getUid(), payload.getHeader().getAppContentID(), response.getStatus()).build();
            loggingUtil.logWSCallSent(logInfo);
        } catch (Exception ex) {
            payload.getHeader().getPromotedFields().put(Constants.PromotedFieldKeys.INTERNAL_MESSAGE, "Exception in logging message on Kibana");
            loggingUtil.logExceptionFromPayload(payload,ex);
            System.err.println("Exception in logging message on Kibana: " + ex.getMessage());
        }
    }

}
